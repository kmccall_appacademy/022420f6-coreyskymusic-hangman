class Hangman
  attr_reader :guesser, :referee, :board, :missed_letters

  def initialize(players)
      @guesser = players[:guesser]
      @referee = players[:referee]
      @wrong_guesses = 0
      @missed_letters = []
  end

  def play
    setup
    while @wrong_guesses < 6
      display
      take_turn
      if won?
        puts "Guesser wins. That was close!"
        display

        break
      end

    end
    if @wrong_guesses == 6
      puts "Word was #{@referee.require_secret_word}"
      puts "Guesser losses"
      puts hang_image(@wrong_guesses)
    end
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @board = Array.new(secret_word_length)
    @guesser.register_secret_length(secret_word_length)

  end

  def won?
    @board.all?
  end

  def display
    display_board = []
    @board.each do |char|
      if char.nil?
        display_board << "_"
      else
        display_board << char
      end
    end

    display_board = display_board.join(" ")
    print "Missed Letters: #{@missed_letters}"
    puts nil
    puts hang_image(@wrong_guesses)

    puts display_board

  end



  def take_turn
    letter = @guesser.guess(@board)
    indices = @referee.check_guess(letter)
    update_board(letter, indices)
    if indices.empty?
      @missed_letters << letter
      @wrong_guesses += 1
    end
    @guesser.handle_response(letter, indices)
  end


  def update_board(letter, indices)
      indices.each {|idx| @board[idx] = letter}
  end

  def hang_image(wrong_guesses)
    if @wrong_guesses == 0
      puts nil
      puts "  ____"
      puts " |    |"
      puts "      |"
      puts "      |"
      puts "      |"
      puts "      |"
      puts "   ___|___"
      puts nil
    elsif @wrong_guesses == 1
      puts nil
      puts "  ____"
      puts " |    |"
      puts " O    |"
      puts "      |"
      puts "      |"
      puts "      |"
      puts "   ___|___"
      puts nil
    elsif @wrong_guesses == 2
      puts nil
      puts "  ____"
      puts " |    |"
      puts " O    |"
      puts " |    |"
      puts " |    |"
      puts "      |"
      puts "   ___|___"
      puts nil
    elsif @wrong_guesses == 3
      puts nil
      puts "  ____"
      puts " |    |"
      puts " O    |"
      puts "-|    |"
      puts " |    |"
      puts "      |"
      puts "   ___|___"
      puts nil
    elsif @wrong_guesses == 4
      puts nil
      puts "  ____"
      puts " |    |"
      puts " O    |"
      puts "-|-   |"
      puts " |    |"
      puts "      |"
      puts "   ___|___"
      puts nil
    elsif @wrong_guesses == 5
      puts nil
      puts "  ____"
      puts " |    |"
      puts " O    |"
      puts "-|-   |"
      puts " |    |"
      puts "/     |"
      puts "   ___|___"
      puts nil
    else
      puts nil
      puts "  ____"
      puts " |    |"
      puts " O    |"
      puts "-|-   |"
      puts " |    |"
      puts "/\\    |"
      puts "   ___|___"
      puts nil
    end

  end

end

class HumanPlayer

  def guess(board)

    display
    puts "Please enter your guessed letter"
    gets.strip
  end
  def require_secret_word
    puts "What was the word?"
    gets.strip
  end

  def handle_response(letter, indices)
    if indices.empty?
      puts "Your letter was not found on the board"

    else
      puts "The letter #{letter} was found at the following indices #{indices.to_s}"
    end
  end

  def check_guess(letter)
    puts "Player guessed the letter #{letter}"
    puts "Please enter which spaces that letter occurs"
    puts "seperate your numbers by commas. For example,"
    puts "if your word was 'hello' and the letter was 'l'"
    puts "you'd enter 3, 4 . If the letter guess does not "
    puts "appear in your word enter 0"

    indices = gets.strip
    if indices == "0"
      indices = []
    else
      indices = indices.split(",").map{|num| num.to_i - 1}
    end
  end

  def pick_secret_word
    print "Think of a word that will be your secret word. "
    puts "Enter it's length"
    gets.strip.to_i
  end

  def register_secret_length(secret_word_length)
    "The Secret Word is #{secret_word_length} letters long"
  end
end

class ComputerPlayer
  attr_reader :candidate_words

  def initialize(dictionary = "dictionary.txt")
    if dictionary == "dictionary.txt"
      @dictionary = File.readlines("dictionary.txt").map(&:chomp)
    else
      @dictionary = dictionary
    end
  end

  def require_secret_word
    @secret_word
  end

  def handle_response(letter, indices)
    if indices.empty?
      @candidate_words = @candidate_words.select! {|word| word.include?(letter) == false}
    else
      @candidate_words = @candidate_words.select! {|word| usable_word?(word, letter, indices)}
    end
  end

  def usable_word?(word, letter, indices)
    word.chars.each_with_index do |char, idx|
      return false if char == letter && !indices.include?(idx)
      return false if indices.include?(idx) && char != letter
    end
    true
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
     @secret_word.length
  end

  def register_secret_length(secret_word_length)
    @candidate_words = @dictionary.select {|word| word.length == secret_word_length}
  end


  def check_guess(letter)
    indices = []
    @secret_word.downcase.chars.each_with_index  do |char, idx|
      indices << idx if char == letter.downcase
    end
    indices
  end

  def guess(board)

    freq_lets = freq_lets_spots(board)
    letter_count = Hash.new(0)
    freq_lets.each do |char|
      letter_count[char] += 1
    end
    letter_count.sort_by { |char, occurance| occurance}[-1][0]


  end

  def freq_lets_spots(board)
    letters = []
    @candidate_words.each do |word|
      word.chars.each_with_index do |char, idx|
        letters << char if board[idx].nil?
      end
    end
    letters
  end

end

if $PROGRAM_NAME == __FILE__
  print "Guesser: Computer (yes/no)? "
    if gets.chomp.downcase == "yes"
      guesser = ComputerPlayer.new
    else
      guesser = HumanPlayer.new
    end

    print "Referee: Computer (yes/no)? "
    if gets.chomp == "yes"
      referee = ComputerPlayer.new
    else
      referee = HumanPlayer.new
    end

    Hangman.new({guesser: guesser, referee: referee}).play


end
